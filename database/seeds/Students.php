<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Student;

class Students extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create("id_Id");
        $gender = ['male', 'female'];
        $active = [0,1];
        $counter = 0;
        for($i=1; $i<=10; $i++){
        	$student = new Student;
        	$student->name = $faker->name;
        	$student->email = $faker->email;
        	$student->password = bcrypt('student123');
        	$student->gender = $gender[rand(0,1)];
        	$student->active = $active[rand(0,1)];
            $student->save();
        	$counter += 1;
        }
        $this->command->info("Berhasil menambahkan ".$counter." Students");
    }
}
