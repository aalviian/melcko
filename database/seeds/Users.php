<?php

use Illuminate\Database\Seeder;
use App\User;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin -> name = "Alvian";
        $admin -> email = "aalviian@gmail.com";
        $admin -> password = bcrypt('admin123');
        $admin -> save();
 
        $this->command->info("Berhasil menambahkan Alvian sebagai Admin");
    }
}
