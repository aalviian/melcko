<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Course;
use App\Student;

class CoursesStudents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker=Faker::create("id_Id");
        $counter = 0;
        $status = ["done","waiting"];
        for($i=1; $i<=10; $i++){
        	$course_student = new App\CoursesStudent;
        	$course_student->student_id = rand(1,10);
        	$course_student->course_id = rand(1,10);
        	$course_student->code = $faker->ean8."ID".$course_student->id;
        	$course_student->amount = $faker->numberBetween($min = 100000, $max = 450000);
        	$course_student->status = $status[rand(0,1)];
            $course_student->save();
        	$counter += 1;
        }
        $this->command->info("Berhasil menambahkan ".$counter." Payment/Course-Student");
    }
}
