<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Course;

class Courses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker=Faker::create("id_Id");
        $counter = 0;
        for($i=1; $i<=10; $i++){
        	$course = new Course;
        	$course->name = $faker->sentence($nbWords = 3, $variableNbWords = true);
        	$course->description = $faker->sentence($nbWords = 12, $variableNbWords = true);
            $course->save();
        	$counter += 1;
        }
        $this->command->info("Berhasil menambahkan ".$counter." Courses");
    }
}
