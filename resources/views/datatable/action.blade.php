<a class="btn btn-xs btn-success" href="{{ $edit_url }}">Edit</a> |
{!! Form::model($model, ['url' => $delete_url, 'method' => 'delete', 'class' => 'form-inline js-confirm', 'data-confirm' => $confirm_message] ) !!}
{!! Form::submit('Delete', ['class'=>'btn btn-xs btn-danger']) !!}
{!! Form::close()!!}
