<div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Full Name') !!}
	{!! Form::text('name', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama lengkap...']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div> 

<div class= " form-group { !! $errors->has('email') ? 'has-error' : '' !!}">
	{!! Form::label('email', 'Email') !!}
	{!! Form::email('email', null, ['class' =>'form-control', 'placeholder' => 'Ketik email valid...']) !!}
	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('password') ? 'has-error' : '' !!}">
	{!! Form::label('password', 'Password') !!}
	{!! Form::password('password', null, ['class' =>'form-control']) !!}
	{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('gender') ? 'has-error' : '' !!}">
	{!! Form::label('gender', 'Gender') !!}
	{!! Form::select('gender', ['male' => 'male', 'female' => 'female']); !!}
	{!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('active') ? 'has-error' : '' !!}">
	{!! Form::label('active', 'Active') !!}
	{!! Form::select('active', [1 => 1, 0 => 0]); !!}
	{!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>

{!! Form::submit( isset( $model) ? 'Update' : 'Save', [ 'class'=> 'btn btn-primary']) !!}