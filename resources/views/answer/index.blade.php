@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Number1</a></li>
				    <li role="presentation"><a href="#2" aria-controls="2" role="tab" data-toggle="tab">Number2</a></li>
				    <li role="presentation"><a href="#3" aria-controls="3" role="tab" data-toggle="tab">Number3</a></li>
				    <li role="presentation"><a href="#4" aria-controls="4" role="tab" data-toggle="tab">Number4</a></li>
				    <li role="presentation"><a href="#5" aria-controls="5" role="tab" data-toggle="tab">Number5</a></li>
				    <li role="presentation"><a href="#6" aria-controls="6" role="tab" data-toggle="tab">Number6</a></li>
				    <li role="presentation"><a href="#7" aria-controls="7" role="tab" data-toggle="tab">Number7</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="1">
				    	Jumlah : {{ $number1 }}
				    </div>
				    <div role="tabpanel" class="tab-pane" id="2">...</div>
				    <div role="tabpanel" class="tab-pane" id="3">
				    	<table class= "table table-hover">
				    		<thead>
				    			<tr>
					    			<th>Name</th>
					    			<th>Active</th>
					    		</tr>
				    		</thead>
				    		<tbody>
				    			@foreach($number3 as $student)
				    			<tr>
				    				<td>{{ $student->name }}</td>
				    				<td>{{ $student->active }}</td>
				    			</tr>
				    			@endforeach
				    		</tbody>
				    	</table>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="4">
				    	<table class= "table table-hover">
				    		<thead>
				    			<tr>
					    			<th>Name</th>
					    			<th>Active</th>
					    			<th>Course</th>
					    		</tr>
				    		</thead>
				    		<tbody>
				    			@foreach($number4 as $student)
				    			<tr>
				    				<td>{{ $student->name }}</td>
				    				<td>{{ $student->gender }}</td>
				    				<td>
				    				@foreach ($student->courses as $course)
				    					<span class="label label-primary">{{ $course->name }}</span>
				    				@endforeach
				    				</td>
				    			</tr>
				    			@endforeach
				    		</tbody>
				    	</table>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="5">
				    	Total Harga : {{ $number5 }}
				    </div>
				    <div role="tabpanel" class="tab-pane" id="6">
				    	<table class= "table table-hover">
				    		<thead>
				    			<tr>
					    			<th>Name</th>
					    			<th>Active</th>
					    			<th>Status</th>
					    		</tr>
				    		</thead>
				    		<tbody>
				    			@foreach($number6 as $student)
				    			<tr>
				    				<td>{{ $student->name }}</td>
				    				<td>{{ $student->gender }}</td>
				    				<td>{{ $student->status }}</td>
				    			</tr>
				    			@endforeach
				    		</tbody>
				    	</table>	
				    </div>
				    <div role="tabpanel" class="tab-pane" id="7">
				    	The highest amount of payment :
				    	Name : {{ $number7 }}
				    </div>
				  </div>

				</div>
			</div>
		</div>
	</div> 
@endsection 