<div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Title') !!}
	{!! Form::text('name', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama buku...']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div> 

<div class= " form-group { !! $errors->has('description') ? 'has-error' : '' !!}">
	{!! Form::label('description', 'Description') !!}
	{!! Form::textarea('description', null, ['class' =>'form-control', 'placeholder' => 'Ketik deskripsi buku...']) !!}
	{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>	


{!! Form::submit( isset( $model) ? 'Update' : 'Save', [ 'class'=> 'btn btn-primary']) !!}