@extends('layouts.app') 
	@section('content')
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="{{ url('/home') }}">Dashboard</a></li>
						<li><a href="{{ route('courses.index')  }}">Course</a></li>
						<li class="active">Edit Course</li>
					</ul>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 class="panel-title">Edit Course</h2>
						</div>
						<div class="panel-body">
							{!! Form::model($course, ['url' => route('courses.update', $course->id),
							'method'=>'put', 'class'=>'form-horizontal']) !!}

							@include('courses.form')

							{!! Form::close() !!}
						</div> 
					</div>
				</div>
			</div>
		</div>
	@endsection