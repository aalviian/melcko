$(document).ready(function () {
// confirm delete
	$(document.body).on('click', '.js-confirm', function (event) {
		event.preventDefault() 
		var $form = $(this).closest('form')
		var $el = $(this)
		var text = $el.data('confirm-message') ? $el.data('confirm-message') : 'You want to delete this one'

		swal({
		  title: 'Are you sure?',
		  text: text,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#DD6B55',
		  confirmButtonText: 'Agree',
		  cancelButtonText: 'Cancel',
		  closeOnConfirm: true
	},
	  function () {
	    $form.submit() 
	  })
	}) 
}); 