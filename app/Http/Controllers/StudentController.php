<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Input;
use Session; 
use Validator;  

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if($request -> ajax()) {
            $students = Student::select('name','email','gender','active');
            return Datatables::of($students) -> addColumn('action', function($student){
                return view('datatable.action', [
                    'model'    => $student,  
                    'delete_url' => route('students.destroy', $student->id),
                    'edit_url' => route('students.edit', $student->id),   
                    'confirm_message' => 'Yakin mau menghapus ' . $student->name . '?',
                ]);
            }) -> make(true);
        }
        $html = $htmlBuilder
                ->addColumn(['data' => 'name', 'name'=>'name', 'title' => 'Name'])
                ->addColumn(['data' => 'email', 'name'=>'email','title'=>'Email'])
                ->addColumn(['data' => 'gender', 'name'=>'gender','title'=>'Gender'])
                ->addColumn(['data' => 'active', 'name'=>'active','title'=>'Active'])
                ->addColumn(['data' => 'action', 'name'=>'action','title'=>'','orderable'=>false,'searchable'=>false]);

        return view('students.index') -> with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255|unique:students',
            'email' => 'required|string|email|max:255|unique:students',
            'password' => 'required|min:6',
            'gender' => 'required|string',
            'active' => 'required|integer'
        ];

        $messages = [
            'required' => 'Data tidak boleh kosong',
            'email' => 'Email tidak valid',
            'email.unique' => 'Email sudah ada dalam database.',
            'password.min' => 'Password minimal 6 karakter',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect() -> route('students.create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $student = new Student;
        $student->name = $request->name;
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->gender = $request->gender;
        $student->active = '1';
        $student->save();

        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan ".$student -> name." kedalam database"]);
        return redirect() -> route('students.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string|max:255|unique:students,name,'.$id,
            'email' => 'required|string|email|max:255|unique:students,email,'.$id,
            'password' => 'required|min:6',
            'gender' => 'required|string',
            'active' => 'required|integer'
        ];

        $messages = [
            'required' => 'Data tidak boleh kosong',
            'email' => 'Email tidak valid',
            'email.unique' => 'Email sudah ada dalam database.',
            'password.min' => 'Password minimal 6 karakter',
        ];

        $student = Student::find($id);
        $old_name = $student->name;

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect() -> route('students.create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $student->update($request->all());
         Session::flash("flash_notif",[
            "level" => "success",
            "message" => "Berhasil mengubah ".$old_name." menjadi ".$student->name." kedalam database"
        ]);
        return redirect() -> route('students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $name_student = $student -> name;
        
        if(!Student::destroy($id)) {
            return redirect() -> back();
        }

        Session::flash("flash_notif", [
            "level" => "success",
            "message" => "Siswa bernama ".$name_student." berhasil dihapus"
        ]);
        return redirect()->route('students.index');
    }
}
