<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;
use App\Course;
use App\Instructor;
use DB;

class AnswerController extends Controller
{
    public function index(){
    	$number1 = Student::has('courses','>',1)->count();
    	$number2 = Instructor::where('gender','female')->get();
    	$number3 = Student::where('active',0)->get();
    	$number4 = Student::has('courses','>',2)->get();
    	$student = Student::has('courses','=',3)->first();
    	$number5 = DB::table('course_student')->where('student_id', $student->id)->sum('amount');
    	$student2 = array();
    	$number6 = DB::table('students')
	            ->join('course_student', 'students.id', '=', 'course_student.student_id')
	            ->where('status','waiting')
	            ->get();
	    $number7 = DB::table('course_student')->where('status','done')->max('amount');

    	return view('answer.index', compact('number1','number2','number3','number4','number5','number6','number7'));
    }
}
