<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Course;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Input;
use Session; 
use Validator;  


class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index(Request $request, Builder $htmlBuilder)
    {
        if($request -> ajax()) {
            $courses = Course::select('name','description');
            return Datatables::of($courses) -> addColumn('action', function($course){
                return view('datatable.action', [
                    'model'    => $course,  
                    'delete_url' => route('courses.destroy', $course->id),
                    'edit_url' => route('courses.edit', $course->id),   
                    'confirm_message' => 'Yakin mau menghapus ' . $course->name . '?',
                ]);
            }) -> make(true);
        }
        $html = $htmlBuilder
                ->addColumn(['data' => 'name', 'name'=>'name', 'title' => 'Name'])
                ->addColumn(['data' => 'description', 'name'=>'description','title'=>'Description'])
                ->addColumn(['data' => 'action', 'name'=>'action','title'=>'','orderable'=>false,'searchable'=>false]);

        return view('courses.index') -> with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
        ];

        $messages = [
            'required' => 'Data tidak boleh kosong',
            'max' => 'Jumlah kata tidak melebihi 255 kata',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect() -> route('courses.create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $course = new Course;
        $course->name = $request->name;
        $course->description = $request->description;
        $course->save();

        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan ".$course -> name." kedalam database"]);
        return redirect() -> route('courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
        return view('courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
