<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $fillable = [
        'name', 'description'
    ];

    public function students(){
    	return $this->belongsToMany('App\Student','course_student','student_id','course_id');
    }
}
