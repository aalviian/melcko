<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'active'
    ];

    public function courses(){
        return $this->belongstoMany('App\Course','course_student','student_id','course_id');
    }

}
 