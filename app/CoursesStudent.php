<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoursesStudent extends Model
{
    public $table = 'course_student';

    protected $fillable = [
        'student_id', 'course_id', 'code', 'amount', 'date', 'status'
    ];
}
